﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CryptoLesson
{
    class SHA256Encrypt
    {
        static string dynamicSalt=null, staticSalt=null;
        public static string getHashSha256(string text, string dynamicSaltArg, byte[] staticSaltArg)
        {
            dynamicSalt = dynamicSaltArg;
            staticSalt = BitConverter.ToString(staticSaltArg);
            var hashWithSalt = text + dynamicSaltArg + staticSalt;
            byte[] bytes = Encoding.Unicode.GetBytes(hashWithSalt);
            SHA256Managed hashString = new SHA256Managed();
            byte[] hashByte = hashString.ComputeHash(bytes);
            string hash = string.Empty;
            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        public static void checkHashSha256(string pass, string text)
        {
            var hashWithSalt = pass + dynamicSalt + staticSalt;
            byte[] bytes = Encoding.Unicode.GetBytes(hashWithSalt);
            SHA256Managed hashString = new SHA256Managed();
            byte[] hashByte = hashString.ComputeHash(bytes);
            string hash = string.Empty;
            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            Console.WriteLine(Equals(hash, text) ? "Equals hash: Hash correct" : "Equals hash: Hash not correct");

        }
    }
}
