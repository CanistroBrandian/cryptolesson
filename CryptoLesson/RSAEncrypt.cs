﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CryptoLesson
{

    public class RSAEncrypt
    {
        private static byte[] publicKey, privateKey, decrypted, encrypted, byteData;
        private const int KEY_SIZE = 2048;


        public static byte[] EncryptData(string data)
        {
            byteData = Encoding.UTF8.GetBytes(data);
            using (var rsa = new RSACryptoServiceProvider(KEY_SIZE))
            {
                publicKey = rsa.ExportCspBlob(false);
                privateKey = rsa.ExportCspBlob(true);
                rsa.ImportCspBlob(publicKey);
                encrypted = rsa.Encrypt(byteData, true);
            }
            return encrypted;
        }

        public static string DecrypteData()
        {
            using (var rsa = new RSACryptoServiceProvider(KEY_SIZE))
            {
                rsa.ImportCspBlob(privateKey);
                decrypted = rsa.Decrypt(encrypted, true);
            }
            return Convert.ToString(Encoding.UTF8.GetString(decrypted));
        }

        public static void CheckEncrypt()
        {
            Console.WriteLine(byteData.SequenceEqual(decrypted) ? "Equals RSA Encrypte: Данные успешно расшифрованы" 
                : "Equals RSA Encrypte: Ошибка при расшифровке");
        }

    }
}
