﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoLesson
{
    class User
    {
        public User()
        {
            this.Id = 1;
            this.Email = "mail.ru";
            this.Pass = "qwerty";
            this.DOB = "04.01.1996";
            this.Phone = "+375291488923";
        }
        public int Id { get; set; }
        public string Email { get; set; }
        public string Pass { get; set; }
        public string DOB { get; set; }
        public string Phone { get; set; }


    }
}
