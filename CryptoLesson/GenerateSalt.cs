﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CryptoLesson
{
    class GenerateSalt
    {

        private static int saltLengthLimit = 32;
        public static byte[] GetSalt()
        {
            return GetSalt(saltLengthLimit);
        }
        public static byte[] GetSalt(int maximumSaltLength)
        {
            var salt = new byte[maximumSaltLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }

            return salt;
        }
    }
}

