﻿using System;
using System.Security.Cryptography;

namespace CryptoLesson
{
    class Program
    {
        static void Main(string[] args)
        {
            var staticSalt = GenerateSalt.GetSalt();
            User asd = new User();

            //реализация Хэширования пароля с динамическим и статической солью заменить на sha256
            Console.WriteLine("MD5Hash: " + MD5Hash.GetMd5Hash(asd.Pass, asd.Phone, staticSalt));
            Console.WriteLine("SHA256: " + SHA256Encrypt.getHashSha256(asd.Pass, asd.Phone, staticSalt));
            SHA256Encrypt.checkHashSha256(asd.Pass,SHA256Encrypt.getHashSha256(asd.Pass, asd.Phone, staticSalt));

            //Реализация RSA шифрования данных 
            RSAEncrypt.EncryptData(asd.Pass);
            RSAEncrypt.DecrypteData();
            RSAEncrypt.CheckEncrypt();

            //Реализация  AES  
            using (RijndaelManaged myRijndael = new RijndaelManaged())
            {
                myRijndael.GenerateKey();
                myRijndael.GenerateIV();
                byte[] encrypted = AESEncrypte.EncryptStringToBytes(asd.Pass, myRijndael.Key, myRijndael.IV);
                Console.WriteLine("Original AES:   {0}", asd.Pass);
                Console.WriteLine("Decrypte AES: {0}", AESEncrypte.DecryptStringFromBytes(
                    encrypted, myRijndael.Key, myRijndael.IV));
            }
        }
    }
}
